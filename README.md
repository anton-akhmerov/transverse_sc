Minimal implementation of the 2DEG setup of Phys. Rev. B 92, 014514 (2015).


Authors: Andre Melo, Anton Akhmerov

License: Public domain